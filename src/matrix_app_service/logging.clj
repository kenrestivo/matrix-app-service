(ns matrix-app-service.logging
  (:require [clojure.tools.trace :as trace]
            [mount.core :as mount]
            [taoensso.timbre :as log]
            [utilza.misc :as umisc]
            [utilza.log :as ulog]
            [taoensso.timbre.appenders.core :as appenders]
            [utilza.java :as ujava]))

(def group-id "matrix-app-service")
(def artifact-id "matrix-app-service")


(defn get-revision
[]
(:version (ujava/get-project-properties group-id artifact-id)))


(defn start-logger
  [{:keys [log] :as config}]
  (let [{:keys [spit-filename appenders tracing?]} log]
    (println "starting logging")
    (log/merge-config! (merge log
                              {:output-fn (partial log/default-output-fn {:stacktrace-fonts {}})
                               :appenders (select-keys {:println (appenders/println-appender {:enabled? false})
                                                        :spit (appenders/spit-appender
                                                               {:fname spit-filename})}
                                                       appenders)}))

    (when tracing? 
      (alter-var-root #'clojure.tools.trace/tracer (fn [_]
                                                     (fn [name value]
                                                       (log/debug name value)))))
    (log/info "Welcome to matrix-app-service" (ujava/revision-info group-id artifact-id))
    (log/info "logging started" (umisc/redact-keys config [:password]))

    log)) ;; so it gets into the state



(mount/defstate log 
  :start (->> (mount/args) start-logger)
  :stop (log/info "Shutting down logger"))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(comment



  )
