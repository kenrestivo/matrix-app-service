(ns matrix-app-service.api
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [yada.yada :as y]
            [bidi.bidi :as b]
            [schema.core :as s]
            [taoensso.timbre :as log]
            [cheshire.core :as json]
            [clj-http.client :as client]))


(defonce db-state (atom nil))

(defn tx
  [db]
  (y/resource {:parameters {:path {:id s/Int}}
               :produces [{:media-type #{"application/json"}
                           :charset "UTF-8"}]
               :consumes [{:media-type #{"application/json"}
                           :charset "UTF-8"}]
               :methods {:put {:response
                               (fn [ctx]
                                 (some-> ctx :parameters :body slurp log/info)
                                 (some-> ctx :parameters :path :id))}}}))


(defn create-user
  [db]
  (y/resource {:parameters {:path {:uid String}}
               :produces [{:media-type #{"application/json"}
                           :charset "UTF-8"}]
               :methods {:get {:response
                               (fn [ctx]
                                 ;; TODO: have to create the user! means reporting them present
                                 (some-> ctx :parameters :path :uid b/url-decode vector))}}}))


(defn get-room
  [db]
  (y/resource {:parameters {:path {:alias String}}
               :produces [{:media-type #{"application/json"}
                           :charset "UTF-8"}]
               :methods {:get {:response
                               (fn [ctx]
                                 ;; TODO: there's only one room, all others must fail.
                                 (some-> ctx :parameters :path :alias b/url-decode vector))}}}))


(defn api-routes
  [db]
  [["/ping" (y/as-resource "pong\n")]
   ["/transactions" [[["/" :id] (tx db)]]]
   ["/users" [[["/" [#".+" :uid]] (create-user db)]]]
   ["/rooms" [[["/" [#".+" :alias]] (get-room db)]]]
   ])


(defn app
  [db]
  ["" [["/" (y/as-resource  "Nothing here\n")]
       ["/foobar" (y/as-resource "whut")]
       ["/api" (api-routes db)]
       ]])

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(comment

  (log/info "foo")

  (ulog/spewer
   (api-routes db-state))

  (ulog/spewer
   (swaggered-api-routes db-state))
  
  (ulog/spewer
   (app db-state))

  (->> "/api/rooms/foo%3Abar"
       (b/match-route (app nil))
       ulog/spewer)

  (->> "/api/users/foo%3Abar"
       (b/match-route (app nil))
       ulog/spewer)

  (->> "/api/transactions/4"
       (b/match-route (app nil))
       ulog/spewer)



  )
