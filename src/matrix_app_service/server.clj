(ns matrix-app-service.server
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [mount.core :as mount]
            [taoensso.timbre :as log]
            [yada.yada :as y]
            [matrix-app-service.logging :as alog] ;; for dependency
            [matrix-app-service.api :as api]
            ))

(defonce db-state (atom nil))
(defonce fake-server (atom nil))


(defn start!
  [settings]
  (log/info "starting up web server" settings)
  (y/listener (api/app db-state) settings))


(defn stop!
  [{:keys [close]}]
  (log/info "Shutting down web server")
  (close))

(mount/defstate ^{:on-reload :noop}
  server 
  :start (->> (mount/args) :web start!)
  :stop (stop! server))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(comment

  ;; LAAAZY, just use mount yo
  (def foo (:close server))

  (log/info "WTF")

  (foo)

  defmacroq

  )
