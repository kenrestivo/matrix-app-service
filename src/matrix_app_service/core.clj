(ns matrix-app-service.core
  (:gen-class)
  (:import [java.net URLEncoder])  
  (:require [utilza.repl :as urepl]
            [utilza.log :as ulog]
            [cheshire.core :as json]
            [taoensso.timbre :as log]
            [matrix-app-service.logging :as alog]
            [matrix-app-service.server :as srv]
            [mount.core :as mount]
            [clj-http.client :as client]))



(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


(comment

  (ulog/catcher
   (mount/stop)
   (mount/start-with-args {:web {:port 8080}
                           :log {:spit-filename "/tmp/foo.log4j"
                                 :tracing? true
                                 :appenders [:spit :println]}}))

  (log/info "foo")


  )
